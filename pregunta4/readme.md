Pregunta 4:
Hacer un API REST que tenga la funcion de listar las carpetas y archivos y ordenar por tamaño o fecha

Uso del API
Link del API ejecutando en ambiente local: http://localhost:5000/api/get_content
Método: POST
Formulario:
    Campo obligatorio: 
        path: <path of directory>
            Ejemplo:
            path: C:\Users\jesus\OneDrive\Escritorio\infotech_test
            Por defecto el campo order es 'size' y el campo reverse_order es 'True'
    
    Campos opcionales:
        order: <size for default or modification_date>
            Ejemplo:
            order: modification_date
        reverse_order: <True (descending) for default or False (ascending)>
            Ejemplo:
            reverse_order: False