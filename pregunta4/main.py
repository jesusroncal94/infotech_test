from flask import Flask, request
from dotenv import load_dotenv
from pathlib import Path
from routes import route_api


env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)
app = Flask(__name__)

# Routes
route_api.routes(app)

if __name__ == '__main__':
    app.run