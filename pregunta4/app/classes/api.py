from helpers import helper
import os
import datetime
import operator


class Api():
    def order_directory(self, path,  order, reverse_order, app=None):
        try:
            folder_path = path
            directory = None
            directory_sizes = []
            content = []

            for directory in os.listdir(folder_path):
                directory = os.path.join(folder_path, directory)
                directory_size = self.get_directory_size(directory)
                directory_sizes.append(directory_size)
                content.append({
                    'name': os.path.basename(directory),
                    'size': self.get_size_format(directory_size),
                    'creation_date': datetime.datetime.fromtimestamp(os.path.getctime(directory)).strftime(f'%Y/%m/%d %X'),
                    'modification_date': datetime.datetime.fromtimestamp(os.path.getmtime(directory)).strftime(f'%Y/%m/%d %X'),
                    'path': directory
                })

            data = {
                'name': os.path.basename(folder_path),
                'size': self.get_size_format(sum(directory_sizes)),
                'creation_date': datetime.datetime.fromtimestamp(os.path.getctime(folder_path)).strftime(f'%Y/%m/%d %X'),
                'modification_date': datetime.datetime.fromtimestamp(os.path.getmtime(folder_path)).strftime(f'%Y/%m/%d %X'),
                'path': folder_path,
                'content': sorted(content, key=operator.itemgetter(order), reverse=reverse_order)
            }
            message = f'Directories and files got successfully from the path {path}'

            return helper.handler_response(app, 200, message, data)
        except Exception as e:
            message= f'Oops! {e.__class__.__name__} occurred. {e}'
            return helper.handler_response(app, 409, message)
    
    def get_directory_size(self, directory):
        total = 0
        try:
            for entry in os.scandir(directory):
                if entry.is_file():
                    total += entry.stat().st_size
                elif entry.is_dir():
                    total += self.get_directory_size(entry.path)
        except NotADirectoryError:
            return os.path.getsize(directory)
        except PermissionError:
            return 0
        return total
    
    def get_size_format(self, b, factor=1024, suffix="B"):
        for unit in ["", "K", "M", "G", "T", "P", "E", "Z"]:
            if b < factor:
                return f"{b:.2f}{unit}{suffix}"
            b /= factor
        return f"{b:.2f}Y{suffix}"