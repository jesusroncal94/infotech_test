from flask import json


def handler_response(app, status_code, output, payload = None):
    if payload is None:
        payload = {}
    response_object = {
        'response': {
            'systemMessage': output,
            'apiResponse': payload,
            'statusCode': status_code,
        }
    }

    response = app.response_class(
        response = json.dumps(response_object),
        status = status_code,
        mimetype = 'application/json'
    )

    return response