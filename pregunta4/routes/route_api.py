from flask import request
from app.classes.api import Api


api = Api()

def routes(app):    
    @app.route('/api/get_content', methods=['POST'])
    def api_get_content():
        values = request.values
        path = values.get('path')
        order = values.get('order') if values.get('order') is not None else 'size'
        reverse_order = False if values.get('reverse_order') == 'False' else True
        return api.order_directory(path, order, reverse_order, app)