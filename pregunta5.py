"""
Pregunta 5:
Escribir una funcion sum() y una función multip() que sumen y multipliquen
respectivamente todos los números de una lista.
Por ejemplo: sum([1,2,3,4]) debería devolver 10 y multip([1,2,3,4]) debería devolver 24.
"""

import random
import numpy


def sum_list(list):
    return sum(list)

def multip_list(list):
    return numpy.prod(numbers)

try:
    len_numbers = int(input('Please specify the number of numbers in the list: '))
    if (len_numbers <= 0): raise Exception('Please the number must be greater than 0.')        
    min_number = int(input('Please specify the minimum number: '))
    max_number = int(input('Please specify the maximum number (greater than minimum): '))
    numbers = list(map(lambda n: random.randint(min_number, max_number), range(len_numbers)))

    print(f'List of {len_numbers} random numbers: {numbers}')
    print(f'Sum: {sum_list(numbers)}')
    print(f'Product: {multip_list(numbers)}')
except Exception as e:
    print(f'Oops! {e.__class__.__name__} occurred. {e}')