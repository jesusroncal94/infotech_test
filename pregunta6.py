"""
Pregunta 6:
Crear una funcion que determine si dado una serie de parentesis, estas se encuentran en pares, es decir,
abierto '(' y cerrado ')'.
Ejm:
 Entrada: '(()()())()()(())'
 Salida: True

 Entrada: '(()('
 Salida: False
"""

def check_parentheses(string_to_verify=''):
    data = list(string_to_verify)
    count_first_element = data.count('(')
    count_second_element = data.count(')')

    if (count_first_element == count_second_element and ('(' in data or ')' in data)):
        return True
    else:
        return False

try:
    string_to_verify = input('Please enter only a series of parentheses (otherwise it will return False): ')
    check_string = check_parentheses(string_to_verify)
    print(f'Entrada: {string_to_verify}')
    print(f'Salida: {check_string}')
except Exception as e:
    print(f'Oops! {e.__class__.__name__} occurred. {e}')