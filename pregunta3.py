"""
Pregunta 3:
Que es INDEX (indice) en base de datos, indicar beneficios y desventajas
"""

"""
Es una estructura de datos enfocado a una o más columnas de una tabla
para la realización de operaciones sobre ésta de forma más eficiente.
Por ejemplo, para la obtención de un registro en un menor tiempo.

Beneficios:
- Mejora el rendimiento de las consultas, especialmente en las de tipo SELECT.
- Evita el escaneo completo de las tablas al realizar las consultas.

Desventajas:
- Uso adicional de recursos en operaciones de escritura.
- La base de datos requerirá mayor espacio para su uso.
"""