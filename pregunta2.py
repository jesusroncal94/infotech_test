"""
Pregunta 2:
Escribir un codigo en python que sume y multiplique respectivamente
todos los numeros de una lista, ejemplo;
Numeros=1 2 3 4, suma 10, multiplicacion 24.
"""

import random
import numpy


try:
    len_numbers = int(input('Please specify the number of numbers in the list: '))
    if (len_numbers <= 0): raise Exception('Please the number must be greater than 0.')        
    min_number = int(input('Please specify the minimum number: '))
    max_number = int(input('Please specify the maximum number (greater than minimum): '))
    numbers = list(map(lambda n: random.randint(min_number, max_number), range(len_numbers)))
    sum_numbers = sum(numbers)
    prod_numbers = numpy.prod(numbers)

    print(f'List of {len_numbers} random numbers: {numbers}')
    print(f'Sum: {sum_numbers}')
    print(f'Product: {prod_numbers}')
except Exception as e:
    print(f'Oops! {e.__class__.__name__} occurred. {e}')
