from rest_framework import routers
from apps.api import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
