Pregunta 8:
Hacer un servicio API Rest en python usando Django y Django Rest Framework que devuelva la hora actual en formato ISO.
Ejemplo:

{
    "hora_actual": "2017-05-23T16:54:00Z"
}

Uso del API
Link del API ejecutando en ambiente local (dependerá qué puerto indique al ejecutar): http://localhost:8000/
Método: GET