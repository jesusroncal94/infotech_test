from rest_framework import generics
from rest_framework.response import Response
from django.utils.timezone import localtime

class ApiRoot(generics.GenericAPIView):
    name = 'API'

    def get(self, request, *args, **kwargs):
        return Response({'fecha_hora_actual': localtime()})
