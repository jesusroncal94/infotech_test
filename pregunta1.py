"""
Pregunta 1:
Generar un codigo en python que sume 10 numeros aleatorios
de la siguiente forma: los 5 mas bajos y los 5 mas altos
"""

import random


try:
    min_number = int(input('Please specify the minimum number: '))
    max_number = int(input('Please specify the maximum number (greater than minimum): '))
    numbers = list(map(lambda n: random.randint(min_number, max_number), range(10)))
    high_numbers = sorted(numbers, reverse=True)[0:5]
    low_numbers = sorted(numbers, reverse=False)[0:5]

    print(f'List of 10 random numbers: {numbers}, sum: {sum(numbers)}')
    print(f'List of the 5 highest numbers: {high_numbers}, sum: {sum(high_numbers)}')
    print(f'List of the 5 lowest numbers:: {low_numbers}, sum: {sum(low_numbers)}')
except Exception as e:
    print(f'Oops! {e.__class__.__name__} occurred.')
